package com.example.barbair.controllers;

import com.example.barbair.models.User;
import com.example.barbair.repositories.interfaces.UserRepository;
import com.example.barbair.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_REQUIRED = "The requested resource requires authentication.";
    public static final String AUTHENTICATION_FAILURE_REST = "Invalid email address";
    public static final String AUTHENTICATION_FAILURE_MVC = "Wrong username or password";

    private final UserRepository userRepository;
    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHENTICATION_REQUIRED);
        }


        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userRepository.getByField("email", email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHENTICATION_FAILURE_REST);
        }

    }
}
