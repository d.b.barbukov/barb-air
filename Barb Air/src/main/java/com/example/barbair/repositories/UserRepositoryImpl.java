package com.example.barbair.repositories;


import com.example.barbair.models.User;
import com.example.barbair.repositories.interfaces.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.lang.String.format;

@Repository
public class UserRepositoryImpl extends AbstractCUDRepository<User> implements UserRepository {


    public final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory){
        super(User.class, sessionFactory);
        this.sessionFactory=sessionFactory;
    }

    @Override
    public boolean isDuplicate(String name, String value, int id) {
        try (Session session= sessionFactory.openSession()){
            Query<User> query=session.createQuery(format("from User where %s = :value and id != :id", name), User.class);
            query.setParameter("value", value);
            query.setParameter("id", id);

            List<User> result=query.list();

            return result.size()!=0;
        }
    }
}
