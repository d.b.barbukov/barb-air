package com.example.barbair.repositories.interfaces;

import com.example.barbair.models.Gender;

public interface GenderRepository extends BaseReadRepository<Gender>{
}
