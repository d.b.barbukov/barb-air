package com.example.barbair.repositories;

import com.example.barbair.models.Role;
import com.example.barbair.models.User;
import com.example.barbair.repositories.interfaces.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends AbstractReadRepository<Role> implements RoleRepository {

    public final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(Role.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
