package com.example.barbair.repositories.interfaces;

import com.example.barbair.models.Role;

public interface RoleRepository extends BaseReadRepository<Role> {
}
