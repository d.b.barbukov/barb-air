package com.example.barbair.repositories;

import com.example.barbair.models.Gender;
import com.example.barbair.models.User;
import com.example.barbair.repositories.interfaces.GenderRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class GenderRepositoryImpl extends AbstractReadRepository<Gender> implements GenderRepository {

    public final SessionFactory sessionFactory;

    @Autowired
    public GenderRepositoryImpl(SessionFactory sessionFactory){
        super(Gender.class, sessionFactory);
        this.sessionFactory=sessionFactory;
    }
}
