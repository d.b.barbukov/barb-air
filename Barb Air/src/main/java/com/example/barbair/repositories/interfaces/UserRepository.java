package com.example.barbair.repositories.interfaces;

import com.example.barbair.models.User;

public interface UserRepository extends BaseReadRepository<User>, BaseCUDRepository<User>{

    boolean isDuplicate(String name, String value, int id);

}
