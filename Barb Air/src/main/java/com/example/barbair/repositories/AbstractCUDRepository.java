package com.example.barbair.repositories;

import com.example.barbair.repositories.interfaces.BaseCUDRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractCUDRepository <T> extends AbstractReadRepository <T> implements BaseCUDRepository<T> {

    private final SessionFactory sessionFactory;

    public AbstractCUDRepository(Class<T> clazz, SessionFactory sessionFactory){
        super(clazz, sessionFactory);
        this.sessionFactory=sessionFactory;
    }

    @Override
    public void create (T entity){

        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(T entity){
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id){
        T toDelete=getById(id);
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(toDelete);
            session.getTransaction().commit();
        }
    }
}
