package com.example.barbair.models.dtos;

public class GenderDto {

    private String type;

    public GenderDto(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
