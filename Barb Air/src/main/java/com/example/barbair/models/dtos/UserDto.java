package com.example.barbair.models.dtos;

import com.example.barbair.models.Gender;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {

    public static final String VALID_USERNAME_LENGTH = "Name must be between 4 and 20 symbols";
    public static final String VALID_PASSWORD_LENGTH = "Password must be at least 8 symbols";
    public static final String VALID_PASSWORD_ELEMENTS = "Password must contain capital letter, digit and special symbol";
    public static final String VALID_EMAIL = "Email must be valid";
    public static final String VALID_PHONE_NUMBER = "Phone number must begin with 0 and contain 9 additional digits";


    @Size(min=2, max=20, message = VALID_USERNAME_LENGTH)
    private String firstName;


    @Size(min=2, max=20, message = VALID_USERNAME_LENGTH)
    private String lastName;

    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*., ?]).+$", message = VALID_PASSWORD_ELEMENTS)
    @Size(min = 8, message = VALID_PASSWORD_LENGTH)
    private String password;

    @Pattern(regexp = "^[a-zA-Z0-9_+&*-.]+([a-zA-Z0-9_+&*-.])*@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", message = VALID_EMAIL)
    private String email;

    @Pattern(regexp = "^0\\d{9}$", message = VALID_PHONE_NUMBER)
    private String phoneNumber;


    private String gender;

    public UserDto(){

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
