package com.example.barbair.services;

import com.example.barbair.models.Gender;
import com.example.barbair.repositories.interfaces.GenderRepository;
import com.example.barbair.services.interfaces.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenderServiceImpl implements GenderService {

    private GenderRepository genderRepository;

    @Autowired
    public GenderServiceImpl(GenderRepository genderRepository){
        this.genderRepository=genderRepository;
    }

    @Override
    public Gender getById(int id) {
        return genderRepository.getById(id);
    }
}
