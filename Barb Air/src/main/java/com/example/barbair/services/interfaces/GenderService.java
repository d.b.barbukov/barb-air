package com.example.barbair.services.interfaces;

import com.example.barbair.models.Gender;

public interface GenderService {

    Gender getById(int id);


}
