package com.example.barbair.services.mappers;


import com.example.barbair.controllers.AuthenticationHelper;
import com.example.barbair.exceptions.UnauthorizedOperationException;
import com.example.barbair.models.Gender;
import com.example.barbair.models.User;
import com.example.barbair.models.dtos.UserDto;
import com.example.barbair.repositories.interfaces.RoleRepository;
import com.example.barbair.repositories.interfaces.UserRepository;
import com.example.barbair.services.interfaces.GenderService;
import com.example.barbair.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class UserMapper {

    private static final String UNAUTHORIZED_ACTION = "Only admin users are allowed to perform this action";


    private final GenderService genderService;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;


    @Autowired
    public UserMapper(GenderService genderService, RoleRepository roleRepository, UserRepository userRepository, AuthenticationHelper authenticationHelper, UserService userService) {
        this.genderService = genderService;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    //create
    public User fromDto(UserDto userDto){

        User user=new User();
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(userDto.getPassword());
        user.setPhoneNumber(userDto.getPhoneNumber());
        setGender(userDto, user);
        user.setRole(roleRepository.getById(2));
        user.setTickets(new HashSet<>());



        return user;
    }

    private Gender setGender(UserDto userDto, User user){
        if(userDto.getGender().equals("male")){
            user.setGender(genderService.getById(1));
        }else{
            user.setGender(genderService.getById(2));
        }
        return user.getGender();
    }

    //update
    public User fromDtoToObject(int id, UserDto userDto, User user){
        if(!user.getRole().getName().equals("admin") || user.getId()!=id){
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }
        User userToUpdate= userRepository.getById(id);
        userToUpdate.setEmail(userDto.getEmail() == null ? userToUpdate.getEmail() : userDto.getEmail());
        userToUpdate.setPassword(userDto.getPassword() == null ? userToUpdate.getPassword() : userDto.getPassword());
        userToUpdate.setPhoneNumber(userDto.getPhoneNumber() == null ? userToUpdate.getPhoneNumber() : userDto.getPhoneNumber());
        userToUpdate.setFirstName(userDto.getFirstName()==null? userToUpdate.getFirstName() : userDto.getFirstName());
        userToUpdate.setLastName(userDto.getLastName()==null? userToUpdate.getLastName() : userDto.getLastName());
        userToUpdate.setGender(userDto.getGender()==null? userToUpdate.getGender(): setGender(userDto, user));

        return userToUpdate;
    }
}
