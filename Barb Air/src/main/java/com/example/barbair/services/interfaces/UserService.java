package com.example.barbair.services.interfaces;

import com.example.barbair.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll(User user);

    User getById(User user, int id);

    void create (User user);

    void update(User user, User toUpdate);
}
