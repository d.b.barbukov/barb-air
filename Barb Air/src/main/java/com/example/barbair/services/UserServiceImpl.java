package com.example.barbair.services;

import com.example.barbair.exceptions.DuplicateEntityException;
import com.example.barbair.exceptions.UnauthorizedOperationException;
import com.example.barbair.models.User;
import com.example.barbair.repositories.interfaces.UserRepository;
import com.example.barbair.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {

    private final static String INVALID_PASSWORD = "Password must be at least 8 symbols and should contain capital letter, digit and special symbol";
    private final static String INVALID_EMAIL = "Invalid email address";
    private final static String INVALID_PHONE_NUMBER = "Phone number must begin with 0 and contain 9 additional digits";

    private final static String DUPLICATE_USER_DATA = "User with this username, email or phone number already exists in the system";

    private static final String UNAUTHORIZED_ACTION = "Only admin users are allowed to perform this action";

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll(User user) {
        throwIfUserIsNotAdmin(user);
        return userRepository.getAll();
    }

    @Override
    public User getById(User user, int id) {
        throwIfUserIsNotAdmin(user);
        return userRepository.getById(id);
    }

    @Override
    public void create(User user) {
        validateUser(user);
        checkForDuplicity(user);
        userRepository.create(user);

    }

    @Override
    public void update(User user, User toUpdate) {
        if (!user.getRole().equals("admin") && user.getId()!= toUpdate.getId()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }

        validateUser(toUpdate);
        checkForDuplicity(toUpdate);
        userRepository.update(toUpdate);
    }


    private void validateUser(User user) {
        //validate fields
        validatePassword(user.getPassword());
        validateEmail(user.getEmail());
        validatePhoneNumber(user.getPhoneNumber());
    }


    private void checkForDuplicity(User user) {
        // check whether method is called from create or update
        String methodCaller = Thread.currentThread().getStackTrace()[2].getMethodName();
        boolean containsDuplicateUsername = false;
        int id= user.getId();
        //username cannot be change once created

        boolean containsDuplicatePhoneNumber = userRepository.isDuplicate("phoneNumber", user.getPhoneNumber(), id);
        boolean containsDuplicateEmail = userRepository.isDuplicate("email", user.getEmail(), id);

        if (containsDuplicateUsername || containsDuplicatePhoneNumber || containsDuplicateEmail) {
            throw new DuplicateEntityException(DUPLICATE_USER_DATA);
        }
    }


    private void validatePassword(String password) {
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*., ?]).+$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(password);

        if (!m.matches() || password.length() < 8) {
            throw new IllegalArgumentException(INVALID_PASSWORD);
        }
    }

    //Taken from OWASP Validation Regex Repository for validating email-addresses
    private void validateEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-.]+([a-zA-Z0-9_+&*-.])*@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern p = Pattern.compile(emailRegex);
        Matcher m = p.matcher(email);

        if (!m.matches()) {
            throw new IllegalArgumentException(INVALID_EMAIL);
        }
    }

    private void validatePhoneNumber(String phoneNumber) {
        String phoneRegex = "^0\\d{9}$";
        Pattern p = Pattern.compile(phoneRegex);
        Matcher m = p.matcher(phoneNumber);

        if (!m.matches()) {
            throw new IllegalArgumentException(INVALID_PHONE_NUMBER);
        }
    }

    private void throwIfUserIsNotAdmin (User user){
        if(!user.getRole().getName().toLowerCase().equals("admin")){
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }
    }
}
